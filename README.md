# TEMA: "Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)" 
```plantuml 
@startmindmap 
*[#Blue] Programando ordenadores \nen los 80 y ahora 

 *[#lightblue] Sistemas antiguos
  *_ lenguajes
   *[#7FFFD4] Se ocupaba lenguaje ensamblador para que la maquina fuera lo mas rapida posible \n ya que no se contaba con la potencia suficiente 
  *_ ejemplos
   *[#7FFFD4] Aparece en 1982 ZX Spectrum
   *[#7FFFD4] Aparece en 1985 MC X2
  *_ ¿Que es un sistema antiguo?
   *[#87CEEB] Es cualquier sistema que ya no este a la venta  \ny que no tenga continuidad en generaciones posteriores
    *[#B0C4DE] Ordenadores de los 80s y 90s

  *_ Diferencias con los actuales
   *[#00FFFF] La potencia que tenian las maquinas
   *[#00FFFF]  La velocidad se media en MHz
   *[#00FFFF]  Las maquinas eran iguales en potencia pero diferentes en aspecto
   *[#00FFFF]  Predominaba el lenguaje ensamblador
   
 *[#lightblue] Sistemas Actuales 
  *_ ¿Que es un sistema actuales?
   *[#87CEEB] Es cualquier sistema actual y \nque se seguira vendiendo en años posteriores

  *_ Diferencias con los antiguos
   *[#00FFFF] La potencia que tenian las maquinas
   *[#00FFFF] La velocidad se media en GHz
   *[#00FFFF] Las maquinas son distintas en todos los sentidos
   *[#00FFFF] Diversidad de lenguajes alto nivel 
 *[#lightblue] Lenguajes
  *[#B0E0E6] Es una serie de instrucciones que ayuda a comunicar al hombre y la maquina
  *[#00CED1] Alto Nivel
   *[#lightblue] Este lenguaje se transforma a \nlenguaje maquina para ser interpretado por la misma
   *_ ejemplos
    *[#00BFFF] Java
     *_ ¿Que es?
      *[#7FFFD4] Es un lenguaje de programacion, la traduccion del codigo se hace de por \nmedio de una maquina virtual para convertirlo en lenguaje maquina
     *[#00BFFF] Unity 
      *_ Se encarga
       *[#00BFFF]  De crear programas para varias plataformas
        *_ Ocupa
         *[#00BFFF]  Librerias
         *[#00BFFF]  Drivers
  *[#00CED1] Bajo Nivel
   *[#00BFFF] Se le conoce como lenguaje ensamblador
    *_ ventajas
     *[#00BFFF] Aprovechas tiempo de ejecucion
     *[#00BFFF] Control total sobre el hardware
   *[#B0E0E6] Es creado por el programador 
 *[#lightblue] Actualidad
  *[#00CED1] Ordenadores Fijos
  *[#00BFFF] Plataformas Moviles
   *[#B0E0E6] Ordenadores Moviles
   *[#B0E0E6] Android
    *_ Diferencia de Hardware
   *[#B0E0E6] Ios
    *_ Diferencia de Hardware
  *[#lightblue] Caso Left Path
   *_ ¿Que es?
    *[#00BFFF] Es liberia(codigo) en javascript de 11 lineas de codigo
   *_ Caso 
    *[#00BFFF] Es el caso en el que se elimino un respositorio en internet que \nocupaban grandes paginas como facebook  
  *[#lightblue] Parches y Actualizaciones
   *_ Algunos Generan
    *[#B0E0E6] Ineficiencia
    *[#B0E0E6] Sobrecarga
    *[#B0E0E6] Mas errores que soluciones
   *_ ¿Que son?
    *[#00BFFF] Son pequeñas mejoras que ayudan a mejorar el funcionamiento o arreglan algun bug de la aplicacion
  @endmindmap 
  ```

  # TEMA: "Hª de los algoritmos y de los lenguajes de programación (2010)" 
```plantuml 
@startmindmap 
*[#Blue] Historia de los algoritmos \n y de los lenguajes de programacion
 *[#lightblue] Programa
  *_ ¿Que es?
   *[#00CED1] Es un conjunto de instrucciones especificas en un lenguaje de programacion \n el computador se encarga de interpretarlo, compilarlo y ejecutarlo
 *[#lightblue] Algoritmos
  *_ Se considera algorito si cumple con:
   *[#00BFFF] Secuencia Ordenada de instrucciones
   *[#00BFFF] Que sea finida
   *[#00BFFF] Definida
  *_ Definicion
   *[#00CED1] Es una lista bien definida de instruciones finitas que permite \nencontrar la suolucion a un problema
    *_ Resiven
     *[#B0E0E6] Una entrada
      *_ La transforman 
       *[#B0E0E6] En una salida
  *_ Se ocuparon en:
   *[#00CED1] La antigua Mesopotamia
    *_ aplicados
     *[#87CEEB] en las Transacciones Comerciales
   *[#00CED1]  En el siglo XVII
    *[#87CEEB] Primeras calculadoras de Bolsillo
   *[#00CED1] En el siglo XX
    *[#87CEEB] Son las maquinas programables
   *[#00CED1] Consiste en una secuencia ordenada \nfinita de pasos
  *_ Se clasifican en:
   *[#00CED1] Polinominales
    *_ se caracterizan 
     *[#87CEEB] Por el tiempo de ejecucion razonable 
   *[#00CED1] No Polinominales
    *_ se caracterizan
     *[#87CEEB] Por el tiempo de ejecucion no razonable 
     *[#00FFFF] Uno de los mas famosos es el \n Problema del Viajero
 *_ surgen  
  *[#lightblue] Las primeras Computadoras
   *[#00BFFF] Se crea la primer computadora 
    *_ propuesta por 
     *[#00CED1] Charles Babbge
      *_ destaco por 
       *[#00CED1] usar una memoria \npara almacenar los calculos
   *[#00BFFF] Se crea la arquitectura de Von Neumman \npropuesta por John Von Neummann 
 *[#lightblue] La informatica
  *_ ¿Que es?
   *[#87CEEB] Es un conjunto de conocimientos técnicos que se \nocupan del tratamiento automático de la información por medio de computadoras.
 *[#lightblue] La ingenieria informatica
  *_ ¿Que es?
   *[#87CEEB] Es la diciplina que se encarga de construir maquinas que ejecutan los programas, \nlenguajes de programacion
 *[#lightblue] Lenguajes de Programacion 
  *[#00CED1] Es la representacion de un algoritmo
   *[#00FFFF] Surgen los lenguajes de programacion \napartir del siglo XX
    *[#00FFFF] Fortran Surge como el primer \nlenguaje de programacion imperativo de alto nivel
     *[#00CED1] Otros lenguajes \nde programacion
      *[#00FFFF] Lips(1960) lenguaje funcional
      *[#00FFFF] Prolog(1971) lenguaje logico
     *[#00CED1] (1971) Surgen Programas concurrentes y paralelos
     *_ Lenguajes Orientados a Objetos
      *[#00FFFF] Simula(1968) 
     *_ Lenguajes Actuales
      *[#00FFFF] Java(1995) Orientado  Objetos
      *[#00FFFF] C++ (1980) Orientado  Objetos
   *_ Cracteristicas
    *[#87CEEB] Se realizaban cartones perforados \nen el siglo XX \nelaborado por Joseph Marie Jacquard
 
 @endmindmap 
 ```


   # TEMA: "Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)" 
```plantuml 
@startmindmap 
*[#Blue] Evolucion de los lenguajes \ny paradigmas de programacion
 *[#87CEEB] Ordenador
  *[#00FFFF] Fue inventado para facilitar el trabajo intelectual
   *_ Interactuan 
    *[#B0E0E6] Diseñador: Define el algoritmo que resuleve el problema
    *[#B0E0E6] Programador: Lo codifica en un lenguaje de programacion \nque el ordenador es capaz de entender
    *[#B0E0E6] El ordenador: Ejecuta el programa
 *[#87CEEB] Paradigmas
  *[#00FFFF] Consiste en la forma de pensar \npara solucinar un problema
  *[#00FFFF] Se empezo con el lenguaje ensamblador para programar periferiscos \no algoritmos que requieran ser veloces
  *_ Otros Paradigmas
   *[#lightblue] Programacion Estructurada
    *[#B0E0E6] Ofrece modularidad
     *[#00FFFF]  dependencias de las arquitecturas
     *_ Ejemplos
      *[#87CEEB] Basic
      *[#87CEEB] Pascal
      *[#87CEEB] C++

   *[#lightblue] Programacion Logica 
    *[#00BFFF] un ejemplo es el lenguaje Prolog
    *[#00FFFF] Es un lenguaje de logica
     *[#00FFFF] Uso de recursividad
     *_ No devuelve
      *[#00BFFF] Numeros
      *[#00BFFF] Letras 
      *[#00BFFF] Operaciones aritmeticas
   *[#lightblue] Programacion Funcional
    *_ caracteristicas
     *[#87CEEB] Se comprueban
     *[#87CEEB] Se verifican
     *[#87CEEB] Usa funciones matematicas
     *[#87CEEB] Es recursivo

    *_ ejemplos:
     *_ ML
     *_ HOPE
     *_ HASKELL
     *_ ERLANG
    *[#00FFFF] Son programas veridicos
     *_ operan 
      *[#00FFFF] Funciones matematicas que devuelven un numero o letra
      *[#00FFFF] Uso de recursividad hasta que cumpla una condicion
   *[#lightblue] Programacion Orientada a objetos
    *_ Consiste en 
     *[#00FFFF] pensar en objetos de la vida real
     *_ Para 
      *[#00FFFF] manipular entradas y salidas distintas
   *[#lightblue] Programacion Distribuida 
    *[#00BFFF]  Aplica comunicaciones entre ordenadores 
     *[#00BFFF]  Se puede emplear a largas distancias
   *[#lightblue] Programacion Orientada a aspectos 
    *[#00BFFF]  Aplica modularizaciones de aplicaciones 
     *[#00BFFF]  Separa responsabilidades 
 *[#87CEEB] Lenguajes de Programacion 
  *[#00CED1] Medio de comunicacion entre el hombre y la maquina
   *[#00BFFF]  Resolver problemas cada mas complejos
   *[#00BFFF]  Adaptarse a las necesidades
   *[#00BFFF]  Eficiencia cada vez mejor al ejecutarse 
 *[#87CEEB] Sistemas Informaticos 
  *_ ¿Que es?
   *[#00BFFF] Es un sistema que permite almacenar y procesar información
    *_ caracteristicas
     *[#87CEEB] Programas
      *[#00FFFF] Se pueden tener en varios ordenadores
 *[#87CEEB] Proyecto  seti
  *_ ¿Que es?
   *[#00BFFF] Es un proyecto lanzado para encontrar vida extraterrestre
    *_ se basa en
     *[#00CED1] Recoger muestras de sondas del espacio
      *_ con
       *[#87CEEB] Radiaciones
        *_ resiven 
         *[#00CED1] mensajes
    
@endmindmap 
```
# "Autor: Lopez Rodriguez Hector Aldahir" 

### Para la elaboracion de estos mapas mentales se usaron los formatos Mindmap y PlantUML .
#### Herramientas
##### PlantUML
[plantuml @ plantuml](https://docs.gitlab.com/ee/administration/integration/plantuml.html)
##### MindMap
[mindmap @ mindmap](https://plantuml.com/es/mindmap-diagram)

